# vrpn_client_ros

This repo was forked from https://github.com/ros-drivers/vrpn_client_ros

The package allows to listen over a VRPN stream for pose information (e.g. from Optitrack Motive), pack data as a PoseStamped message and publish it as a ROS topic.

Additional capabilities: the original code has been modified to convert pose - read on VRPN streaming - from Motive frame (west-up-north) to ROS frame (ENU).

An alternative (and more modular) approach is to use the original vrpn_client_ros package and to set-up a new ROS node which takes the ROS topic streamed by vrpn_client_ros, converts it and publishes a new topic with converted pose to be used by mavros.
However this is implemented in python (not efficient) and introduces lags. So it is more efficient to do the conversion directly in the vrpn_client_ros node (C++) and directly publish the topic with converted pose.

## Installation

Supposing you have a ROS catkin workspace already installed:

    cd <CATKIN_WS>
    rosinstall_generator vrpn_client_ros --rosdistro kinetic --deps --wet-only --exclude-path src --tar > kinetic-vrpn_ros.rosinstall

Open the rosinstall file:

    nano kinetic-vrpn_ros.rosinstall

Modify the rosinstall file as follows in correspondence of the vrpn_client_ros entry:

    - git:
        local-name: vrpn_client_ros
        uri: https://gitlab.com/flyart/vrpn_client_ros.git
        version: kinetic-devel

Update the workspace

    wstool merge -t src kinetic-vrpn_ros.rosinstall
    wstool update -t src

Install dependencies

    rosdep install --from-paths src --ignore-src --rosdistro kinetic -y -r --os=debian:jessie

Compile and install

    sudo ./src/catkin/bin/catkin_make_isolated --install -DCMAKE_BUILD_TYPE=Release --pkg vrpn_client_ros --install-space /opt/ros/kinetic

Source the setup script

    source ./devel_isolated/setup.bash

## Launch file set-up

In order to use vrpn_client_ros, you have to set-up the launch file first.

Go into the installation folder

    roscd vrpn_client_ros

Create a copy of the launch file and open it

    cd launch
    cp sample.launch motive_VRPN.launch
    nano motive_VRPN.launch

The parameters of concern are listed at the top of the launchfile; these are the only parameters which have to be set-up by the user:
* ``vrpn_server`` IP address of the machine where Motive runs and from which data is streamed over VRPN
* ``ns`` the ROS namespace where the vrpn_client_ros node will live. This should ideally be a name to identify the UAV in the ROS network.
* ``motive_asset_name`` the name which identifies the UAV in Motive.

WARNING: make sure that the ``motive_asset_name`` string does not contain dashes; only alphanumeric characters are allowed (case sensitive); this string must be the same which appears in Motive under the Assets Pane.

Specifical parameters of vrpn_client_ros are the following ones (you can find them in the original implementation of the package, see documentation at http://wiki.ros.org/vrpn_client_ros):

* ``server`` IP address of the machine where Motive runs and from which data is streamed over VRPN
* ``port`` VRPN channel port (default 3883)
* ``frame_id`` string defining the reference frame of published data (this should be "map" -without quotes- for ROS to run properly)
* ``broadcast_tf`` true to publish a topic with a tf defining the transform from frame_id to the rigid body frame (we don't need it so we can disable it)
* ``refresh_tracker_frequency`` comment out if you use the ``trackers`` property
* ``trackers`` a list of trackers to listen over: each rigid body defined in Motive has a name associated to it, which has to be listed in the launch file (ideally a drone should listen only to its own pose information)

Additional parameters have been added to the launch file:

* ``convert_to_enu`` true if conversion from the Motive frame to the ROS frame has to be applied (default: false, corresponding to original package behaviour)
* ``relay_topic`` true if you want the pose to be published on a custom topic (default: false, corresponding to original package behaviour)
* ``relay_topic_name``custom topic name (default topic is ``/vrpn_client_ros/<tracker_name>/pose``). Use ``/<tracker_name>/mavros/mocap/pose`` in order to relay the topic to mavros and send it to the FCU. See also https://mzahana.gitbooks.io/indoor-nav-at-risclab/content/feeding-mocap-data-into-pixhawk/getting-mocap-data-into-px4.html

WARNING: ``relay_topic`` and ``relay_topic_name`` parameters are deprecated; use remapping instead (see section Implementation Details).

## Launch file example

The most elegant and formally correct way to write the launch file (from the ROS point of view) is:

    <launch>
      <arg name="vrpn_server" default="192.168.1.100"/>
      <arg name="ns" default="ant1"/>
      <arg name="motive_asset_name" default="ANT1"/>

      <group ns="$(arg ns)">

        <node pkg="vrpn_client_ros" type="vrpn_client_node" name="vrpn_client_node" output="screen">
          <rosparam subst_value="true">
            server: $(arg vrpn_server)
            port: 3883

            update_frequency: 100.0
            frame_id: map

            # Use the VRPN server's time, or the client's ROS time.
            use_server_time: false
            broadcast_tf: true

            # Must either specify refresh frequency > 0.0, or a list of trackers to create
            # refresh_tracker_frequency: 1.0
            trackers:
            - "$(arg motive_asset_name)"

            # Convert to ENU frame: apply conversion from Motive Optitrack frame (WUN) to ROS frame (ENU)
            convert_to_enu: true

            # Relay topic: publish pose on topic different from the default one
            relay_topic: false
            # (if relay_topic is true, use the following name)
            relay_topic_name: /your/topic/name

          </rosparam>
          <remap from="vrpn_client_node/$(arg motive_asset_name)/pose" to="mavros/mocap/pose" />
        </node>
      </group>
    </launch>

It is useful to employ a launchfile to launch multiple nodes (e.g. ``mavros`` and ``flyart_cmd``) as a group of nodes which share the same namespace: see https://gitlab.com/flyart/px4/flyart_mavros/.

Another way to write the launch file (old method, less elegant) is:

    <launch>

      <arg name="server" default="localhost"/>

      <node pkg="vrpn_client_ros" type="vrpn_client_node" name="vrpn_client_node" output="screen">
        <rosparam subst_value="true">
          server: 192.168.0.100
          port: 3883

          update_frequency: 100.0
          frame_id: map

          # Use the VRPN server's time, or the client's ROS time.
          use_server_time: false
          broadcast_tf: false

          # Must either specify refresh frequency > 0.0, or a list of trackers to create
          #refresh_tracker_frequency: 1.0
          trackers:
          - ANT-1

          # Convert to ENU frame: apply conversion from Motive Optitrack frame (WUN) to ROS frame
          convert_to_enu: true

          # Relay topic: publish pose on topic different from the default one
          relay_topic: true
          # (if relay_topic is true, use the following name)
          relay_topic_name: /ant1/mavros/mocap/pose

        </rosparam>
      </node>

    </launch>

## Usage

Once you have set-up the launch file, run

    roslaunch vrpn_client_ros motive_VRPN.launch

## Implementation details

The philosophy of ``vrpn_client_ros`` is originally intended to have only one node which listens to multiple trackers (i.e., UAVs in this case); the information on pose of each UAV is then published on a topic like ``vrpn_client_node/<MOTIVEASSETNAME>/pose``.

We decided instead to have a ``vrpn_client_ros`` node running on each machine, which listens to the VRPN streaming of that specific UAV only; this reduces the lag (avoiding to have a ``vrpn_client_ros`` node running e.g. on the ground PC which listens to the VRPN server and publishing back the pose as a ROS topic to the UAV).

For this reason, we decided to invert the philosophy of ``vrpn_client_ros``: the node is launched within a ROS namespace which identifies the specific UAV, so that the ROS node looks like ``/<RosNamespace>/vrpn_client_ros`` and the pose topic is ``/<RosNamespace>/mavros/mocap/pose``. This helps integration with the other ROS nodes involved (e.g. ``mavros`` and ``flyart_cmd``), which naturally live and publish topics under the same namespace. ROS topic remapping is employed to achieve this (see launch file).

References:
* ROS documentation on resource names http://wiki.ros.org/Names
* ROS guide for beginners (chapters 5 and 6) https://www.cse.sc.edu/~jokane/agitr/

## Maintainers
Simone Panza simone.panza@polimi.it
